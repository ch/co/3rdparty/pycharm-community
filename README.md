# Packaging instructions
## First-time
* Clone <https://gitlab.developers.cam.ac.uk/ch/co/makedeb/> in the parent directory, so that the following symlink `Makefile -> ../makedeb/Makefile` works
* Create symlink to the `Makefile`: `ln -s ../makedeb/Makefile ./Makefile`
* Run `make skel` to create skeleton `ROOT/DEBIAN/control`
* Edit `ROOT/DEBIAN/control` appropriately
* Create symlinks in `ROOT/usr/local/bin/`
* Create desktop file in `ROOT/usr/share/applications/`

## Other times
* Delete old `ROOT/opt/` contents

## Always
* Download upstream PyCharm Community `.tar.gz` file from <https://www.jetbrains.com/pycharm/download/#section=linux>
* Extract into `ROOT/opt/` (the `tar` already has a `pycharm-community-yyyy.major.minor` folder)
* Edit the default paths in `ROOT/opt/pycharm-community-yyyy.major.minor/bin/idea.properties` as follows:

  ```
  idea.config.path=${user.home}/.PyCharmCE2019.3/config
  idea.system.path=/scratch/${user.name}/.PyCharmCE2019.3/system
  ```

# Build instructions
* Run `make build` (this automatically bumps the version number, but does NOT upload anything to our local deb repository yet)
* Run `make upload` (this uploads the `.deb` packages to `downloads.ch.private.cam.ac.uk`)
* Run `make gittag` as instructed by the output from `make upload`
